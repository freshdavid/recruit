import { SignUpOptions } from "../utils/types";

// 基本的响应格式
export interface BaseRes {
  code: string;
  message: string;
  data: string;
}

/**
 * 成功的响应
 */
export type LoginRes = BaseRes & {
  data: string;
};

/**
 * 获取用户信息
 */
export type UserInfoRes = BaseRes & {
  data: {
    id: number;
    name: string;
    phone: string;
    stuNum: string;
    aca: string;
    major: string;
    sex: string;
    dire: string;
    status: number;
    intro: string;
    openid: string;
  };
};

/**
 * 队列的格式
 */
export type LinQueuesRes = BaseRes & {
  data: SignUpOptions[];
};

/**
 * 等待队列数据
 */
export type WaitQueueRes = BaseRes & {
  data: string[];
};

/**
 * 报名响应格式
 */
export type SignUpRes = BaseRes & {
  data: {
    data: "报名成功";
  };
};
