import { getRes, request } from "../utils/request";
import { SignUpOptions } from "../utils/types";
import {
  BaseRes,
  LinQueuesRes,
  LoginRes,
  SignUpRes,
  WaitQueueRes
} from "./types";

/**
 * 获取用户的状态
 */
export const getUserStatus = async (openid: string) =>
  getRes<BaseRes>(
    await request({
      url: `/user/getStatus/${openid}`,
      method: "GET"
    })
  );

/**
 * 登录
 */
export const toLogin = async (code: string) =>
  getRes<LoginRes>(
    await request({ url: `/user/getUserInfo/${code}`, method: "GET" })
  );

/**
 * 校验是否重复登录
 * @param openid
 */
export const checkIsRep = async (openid: string) =>
  getRes<BaseRes>(
    await request({ url: `/user/ifHadSigned/${openid}`, method: "GET" })
  );

/**
 * 报名
 * @param options
 */
export const signUp = async (options: SignUpOptions) =>
  getRes<SignUpRes>(
    await request({ url: "/user/sign", method: "POST", data: options })
  );

/**
 * 排队
 * @param openid
 */
export const wait = async (openid: string) =>
  getRes<BaseRes>(await request({ url: `/user/wait/${openid}` }));

/**
 * 获取排队的人数
 */
export const getLineCount = async (openid: string) =>
  getRes<LinQueuesRes>(await request({ url: `/admin/getWaitQueue/${openid}` }));

/**
 * 获取等待队列
 * @param openid
 */
export const getWaitQueue = async (openid: string) =>
  getRes<WaitQueueRes>(
    await request({ url: `/user/getWaitQueueByOpenid/${openid}` })
  );

/**
 * 查看当天预约的人数
 * @param date
 * @param openid
 */

export const checkOrderNum = async (date: string, openid: string) =>
  getRes<BaseRes>(
    await request({ url: `/user/getOrderCount/${date}/${openid}` })
  );

/**
 * 预约时间
 * @param options
 */
interface orderObject {
  date: string;
  openid: string;
}
export const orderTime = async (options: orderObject) =>
  getRes<BaseRes>(
    await request({ url: "/user/orderTime", method: "POST", data: options })
  );

/**
 * 获取预约时间
 * @param options
 */

export const getOrderTime = async (openid: string) =>
  getRes<BaseRes>(await request({ url: `/user/getTime/${openid}` }));

/**
 * 取消排队
 * @param openid
 */
export const cancelLine = async (openid: string) =>
  getRes<BaseRes>(await request({ url: `/user/cancelWait/${openid}` }));

/**
 * 获取个人信息
 * @param openid
 */
export const getUserInfoById = async (openid: string) =>
  getRes<LinQueuesRes>(
    await request({
      method: "POST",
      url: "/admin/findUserInfo",
      data: { openid }
    })
  );
