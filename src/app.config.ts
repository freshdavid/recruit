export default {
  pages: [
    "pages/status/index",
    "pages/lineUp/index",
    "pages/user/index",
    "pages/result/index",
    "pages/signUp/index",
    "pages/description/index"
  ],
  window: {
    backgroundTextStyle: "dark",
    navigationBarBackgroundColor: "#a29bfe",
    navigationBarTitleText: "WeChat",
    navigationBarTextStyle: "black"
  },
  tabBar: {
    selectedColor: "#a29bfe",
    list: [
      {
        pagePath: "pages/status/index",
        text: "状态",
        iconPath: "assets/images/status.png",
        selectedIconPath: "assets/images/status-active.png"
      },
      {
        pagePath: "pages/user/index",
        text: "个人中心",
        iconPath: "assets/images/user.png",
        selectedIconPath: "assets/images/user-active.png"
      }
    ]
  }
};
