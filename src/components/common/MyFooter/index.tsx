import { Component, createRef } from "react";
import { View, Text, Image } from "@tarojs/components";

import "./index.scss";

// eslint-disable-next-line import/first
import Taro from "@tarojs/taro";

import { error, isLogin, isDeadLineTime, showMes } from "../../../utils/util";

//组件的引入
import { AtIcon, AtMessage } from "taro-ui";

//图片引入
import spaceship from "../../../assets/images/spaceship.png";

export default class Index extends Component {
  state = {
    animationData: {},
    timer: {}
  };
  textRef = createRef();
  toSignUp = event => {
    //超过截止时间
    if (isDeadLineTime(Date.now())) {
      return error("已经截止登录");
    }
    //报名文字的dom
    const { textRef } = this;
    textRef.current.style.opacity = "0";

    //创建动画
    const animation = Taro.createAnimation({
      transformOrigin: "50% 50%",
      duration: 1000,
      timingFunction: "ease"
    });
    animation.bottom("100vh").step();
    this.setState({ animationData: animation.export() });
    //火箭起飞完后
    setTimeout(() => {
      //判断用户是否登陆
      if (!isLogin()) {
        Taro.switchTab({
          url: "/pages/user/index"
        });
        showMes("请先授权登录");
      } else {
        Taro.redirectTo({
          url: "/pages/signUp/index"
        });
      }
    }, 1000);
  };

  componentWillUnmount() {}

  componentDidMount() {}

  componentDidShow() {}

  componentDidHide() {}
  render() {
    return (
      <View className="footer-container">
        <AtMessage />
        <View className="sign-container">
          <View
            className="sign"
            onClick={this.toSignUp}
            animation={this.state.animationData}
          >
            <Image
              src={spaceship}
              className="spaceship-img"
              ref={this.imgRef}
            />
            <Text className="text" ref={this.textRef}>
              我要报名
            </Text>
          </View>
        </View>
        <View className="arrow">
          <AtIcon value="chevron-down" color="#FFF"></AtIcon>
        </View>
      </View>
    );
  }
}
