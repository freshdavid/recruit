import { Component } from "react";
import { View, Text } from "@tarojs/components";

import "taro-ui/dist/style/components/button.scss"; // 按需引入
import "./index.scss";

// eslint-disable-next-line import/first
import Taro from "@tarojs/taro";

export default class Index extends Component {
  state = {
    content: []
  };
  componentWillMount() {}

  componentDidMount() {
    const { text, delay = 2000 } = this.props;
    let i: number = 0;
    setTimeout(() => {
      const timer = setInterval(() => {
        const { content } = this.state;
        this.setState({ content: [...content, text[i]] });
        i += 1;
        if (i == text.length) {
          clearInterval(timer);
        }
      }, 50);
    }, delay);
  }

  componentWillUnmount() {}

  componentDidShow() {}

  componentDidHide() {}
  render() {
    return (
      <View className="word-container">
        {this.state.content.map((item, index) => {
          return (
            <Text key={index} className="word">
              {item}
            </Text>
          );
        })}
      </View>
    );
  }
}
