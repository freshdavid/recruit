import { Component } from "react";
import { View, Text } from "@tarojs/components";
import { AtIcon, AtButton } from "taro-ui";

import "./index.scss";

// eslint-disable-next-line import/first
import Taro from "@tarojs/taro";

//组件的引入
import MyHeader from "../MyHeader";
import Word from "../Word";
import MyFooter from "../MyFooter";
import MyButton from "../MyButton";

export default class Index extends Component {
  state = {};

  componentWillUnmount() {}

  componentDidMount() {}

  componentDidShow() {}

  componentDidHide() {}
  render() {
    return (
      <View className="description-main-container">
        <MyHeader title={this.props.title} />
        <View className="main-content">
          {this.props.contents.map((item, index) => {
            return (
              <View className="content">
                <MyButton title={item.title} />
                <Word text={item.text} key={index} />
              </View>
            );
          })}
        </View>
        <MyFooter />
      </View>
    );
  }
}
