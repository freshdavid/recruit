import { Component } from "react";
import { View, Text, Image } from "@tarojs/components";
import { AtIcon } from "taro-ui";

import "./index.scss";
//引入图片
import earthPng from "../../../assets/images/earth.png";

// eslint-disable-next-line import/first
import Taro from "@tarojs/taro";

export default class Index extends Component {
  state = {};

  componentWillUnmount() {}

  componentDidMount() {}

  componentDidShow() {}

  componentDidHide() {}
  render() {
    const { title = "易动" } = this.props;
    return (
      <View className="header-container">
        <View className="left">
          <View className="star_two star">
            <Text className="vertical-line"></Text>
            <AtIcon
              value="star"
              size="20"
              className="ico-star"
              color="#c695aa"
            />
          </View>
          <View className="star_one star">
            <Text className="vertical-line"></Text>
            <AtIcon
              value="star"
              size="20"
              className="ico-star"
              color="#b3bfbb"
            />
          </View>
        </View>
        <View className="center">
          <Text className="title">{title}</Text>
          <Image src={earthPng} className="earth" />
        </View>
        <View className="right">
          <View className="star_one star">
            <Text className="vertical-line"></Text>
            <AtIcon
              value="star"
              size="20"
              className="ico-star"
              color="#b3bfbb"
            />
          </View>
          <View className="star_two star">
            <Text className="vertical-line"></Text>
            <AtIcon
              value="star"
              size="20"
              className="ico-star"
              color="#c695aa"
            />
          </View>
        </View>
      </View>
    );
  }
}
