import { Component } from "react";
import { View, Text } from "@tarojs/components";

import "./index.scss";

// eslint-disable-next-line import/first
import Taro from "@tarojs/taro";

export default class Index extends Component {
  state = {};

  componentWillUnmount() {}

  componentDidMount() {}

  componentDidShow() {}

  componentDidHide() {}
  render() {
    const { title = "简介" } = this.props;
    return <Text className="my-button-container">{title}</Text>;
  }
}
