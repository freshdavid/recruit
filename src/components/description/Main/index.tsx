import { Component } from "react";
import { View, Text } from "@tarojs/components";
import { AtIcon, AtButton } from "taro-ui";

import "./index.scss";

// eslint-disable-next-line import/first
import Taro from "@tarojs/taro";

import Item from "../../common/Item";

export default class Index extends Component {
  state = {
    contents: [
      {
        title: "简介",
        text:
          "易动工作室成立于2016年，是由广东工业大学多个学院学生组成的校园组织，旨在培养和提高科技文化、在课余时间充实提升自我。"
      },
      {
        title: "优势",
        text:
          "易动工作室注重培养技术人才，在这里你可以充分发挥个人才能，拥有许多对接优秀资源的机会，实现能力和创意变现。"
      }
    ]
  };

  componentWillUnmount() {}

  componentDidMount() {}

  componentDidShow() {}

  componentDidHide() {}
  render() {
    return (
      <View>
        <Item contents={this.state.contents} />
      </View>
    );
  }
}
