import { Component } from "react";
import { View, Text } from "@tarojs/components";
import { AtIcon, AtButton } from "taro-ui";

import "./index.scss";

// eslint-disable-next-line import/first
import Taro from "@tarojs/taro";

import Item from "../../common/Item";

export default class Index extends Component {
  state = {
    contents: [
      {
        title: "简介",
        text: `前端主要是基于html、css、js以及当下流行的框架等编程语言进行网站开发，前端除了实现网站功能之外，还需要注重页面的样式设计和用户交互体验。
         `
      },
      {
        title: "要求",
        text: `对前端前沿技术感兴趣,想要开发独特有创意的网站`
      }
    ]
  };

  componentWillUnmount() {}

  componentDidMount() {}

  componentDidShow() {}

  componentDidHide() {}
  render() {
    return (
      <View>
        <Item contents={this.state.contents} title="前端" />
      </View>
    );
  }
}
