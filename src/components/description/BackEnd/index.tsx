import { Component } from "react";
import { View, Text } from "@tarojs/components";
import { AtIcon, AtButton } from "taro-ui";

import "./index.scss";

// eslint-disable-next-line import/first
import Taro from "@tarojs/taro";

import Item from "../../common/Item";

export default class Index extends Component {
  state = {
    contents: [
      {
        title: "简介",
        text:
          "后台开发，主要负责各类数据的处理和传输，进行逻辑处理和数据交互，现阶段的Web端、移动端、小程序端等都需要后台技术的支持。"
      },
      {
        title: "优势",
        text:
          "易动软件后台组基于java语言开发。如果你对Java编程感兴趣，有出色的学习能力以及良好的团队协作能力，那么快点加入我们吧。"
      }
    ]
  };

  componentWillUnmount() {}

  componentDidMount() {}

  componentDidShow() {}

  componentDidHide() {}
  render() {
    return (
      <View>
        <Item contents={this.state.contents} title="后台" />
      </View>
    );
  }
}
