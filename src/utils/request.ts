import Taro from '@tarojs/taro'


/**
 * 请求，封装成promise
 * @param options
 */

export const request = (options: any, needToken?: boolean): Promise<{ data: { msg: string, data: any, code: number } }> => new Promise((resolve) => {
  Taro.request({
    ...options,
    url: baseURL + options.url,
    success: (res: any) => {
      resolve(res);
    }
  })
})

export const baseURL = 'https://www.aniu.net.cn:8081/recruit'

export const getRes = <T>(data: any): T => data.data;
