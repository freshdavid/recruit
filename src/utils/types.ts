// 请求的配置
export interface RequestOptions {
  url: string
  method?: "POST" | "OPTIONS" | "GET" | "HEAD" | "PUT" | "DELETE" | "TRACE" | "CONNECT",
  data?: object
  header?: object
}

// 用于信息
export interface UserInfo {
  nickName: string
  imgSrc: string
}

/**
 * 报名的字段
 */
export interface SignUpOptions {
  college: string,
  direction: string,
  introduce: string,
  major: string,
  name: string,
  openid: string,
  phoneNum: string,
  qq: string,
  sno: string,
  sex: string
  state?: string;
}
