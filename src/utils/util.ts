import Taro from "@tarojs/taro";
import { SignUpOptions } from "./types";

/**
 * 存入store中
 * @param key
 * @param val
 */
export const setLocalStore = (key: string, val: any) => {
  Taro.setStorageSync(key, isObject(val) ? stringify(val) : val);
};

/**
 * 从localStore获取值
 * @param key
 */
export const getLocalStore = <T>(key: string): T | any => {
  let val: any = Taro.getStorageSync(key);
  if (typeof val === "string") {
    // 如果是字符串类型的对象，先转成对象
    if (val.startsWith("[") || val.startsWith("{")) {
      val = parse(val);
    }
  }
  return val;
};

// 判断是不是对象
export const isObject = (val: any) => typeof val === "object";

/**
 * 序列化
 * @param val
 */
export const stringify = (val: object) => JSON.stringify(val);

/**
 * 将序列化的字符串对象转为对象
 * @param val
 */
export const parse = (val: any) => JSON.parse(val);

/**
 * 成功的消息
 * @param msg
 */
export const success = (msg: string) => {
  Taro.atMessage({
    type: "success",
    message: msg
  });
};

/**
 * 失败的消息
 * @param msg
 */
export const error = (msg: string) => {
  Taro.atMessage({
    type: "error",
    message: msg,
    duration: 1000
  });
};

/**
 * 展示信息
 * @param msg
 */
export const showMes = (msg: string) => {
  Taro.showToast({
    title: msg,
    icon: "none",
    duration: 1000
  });
};

/**
 * 检验登录态
 */
export const validateLoginStatus = (): Promise<{ code: number }> => {
  return new Promise((res, rej) => {
    // 获取token
    const openid = getLocalStore("openid");
    if (!openid) {
      // 没有token, 直接登录
      return res({ code: 0 });
    } else {
      // 检验session_key
      Taro.checkSession({
        success() {
          // token没有失效
          res({ code: 1 });
        },
        fail() {
          // 失效了
          res({ code: 0 });
        }
      });
    }
  });
};

/**
 * 生成报名信息
 * @param current
 */
export const genSigningMsg = (current: number) => {
  // 没有授权登录
  if (!isLogin()) {
    return { title: "报名中", desc: "请先授权登录吧" };
  }
  let title = "",
    desc = "",
    status = "";
  switch (current) {
    case -2:
    case -3:
    case 1:
    case 2:
    case 3:
    case 4:
      title = "报名结束";
      desc = "你已报名";
      status = "success";
      break;
    case 0:
      title = "报名中";
      desc = "快去报名吧";
      break;
  }

  return { title, desc, status };
};

/**
 * 生成面试信息
 * @param current
 */
export const genInterviewMsg = (current: number) => {
  // 没有授权登录
  if (!isLogin()) {
    return { title: "面试", desc: "等待通知" };
  }

  let title = "",
    desc = "",
    status = "";
  switch (current) {
    case -2:
      desc = "请前往结果通知中查看面试结果";
      title = "面试结束";
      status = 'success'
      break;
    case 0:
    case 1:
      title = "面试";
      desc = "等待通知";
      break;
    case 2:
      title = "面试";
      desc = "等待面试结果吧";
      break;
    case -3:
    case 3:
    case 4:
      title = "面试结束";
      desc = "请前往结果通知中查看面试结果";
      status = 'success'
  }

  return { title, desc, status };
};

/**
 * 生成考核信息
 * @param current
 */
export const genAssessmentMsg = (current: number) => {
  // 没有授权登录
  if (!isLogin()) {
    return { title: "考核", desc: "等待通知" };
  }

  let title = "",
    desc = "",
    status = "";
  switch (current) {
    case -2:
    case 0:
    case 1:
    case 2:
    case 3:
      title = "考核";
      desc = "等待通知";
      break;
    case -3:
      title = "考核结束";
      desc = "请前往结果通知中查看考核结果";
      status = 'success'
      break;
    case 4:
      title = "考核结束";
      desc = "请前往结果通知中查看考核结果";
      status = 'success'
  }

  return { title, desc, status };
};

/**
 * 监听事件
 * @param type
 * @param fn
 */
export const $on = (type: string, fn: any) => {
  Taro.eventCenter.on(type, fn);
};

/**
 * 触发事件
 * @param type
 */
export const $emit = (type: string) => {
  Taro.eventCenter.trigger(type);
};

/**
 * 判断是不是一个数组
 * @param v
 */
export const isArray = (v: any) =>
  Array.isArray
    ? Array.isArray(v)
    : Object.prototype.toString.call(v) === "[object Array]";

/**
 * 格式化数组
 * @param data
 */
export const formatData = (data: string[]): SignUpOptions[] => {
  return data.map(item => parse(item));
};

/**
 * 处理状态
 * @param status
 */
export const formatCurrent = (status: number): number => {
  // 没有授权登录，默认返回0
  if (!isLogin()) {
    return 0;
  }

  let s: number;
  switch (status) {
    case -3:
      s = -3;
      break;
    case -2:
      s = -2;
      break;
    case 0:
      s = 0;
      break;
    case 1:
      s = 1;
      break;
    case 2:
      s = 1;
      break;
    case 3:
      s = 2;
      break;
    case 4:
      s = 3;
  }
  return s;
};

// 判断是否授权登录过了
export const isLogin = (): boolean => {
  const info = getLocalStore("userInfo");
  return !!(info && info.imgSrc);
};

/**
 * 确定框
 */
export const confirm = (title: string, content: string): Promise<boolean> => {
  return new Promise(resolve => {
    Taro.showModal({
      title,
      content,
      success: function (res) {
        if (res.confirm) {
          resolve(true);
        } else if (res.cancel) {
          resolve(false);
        }
      }
    });
  });
};

/**
 * 比较到指定的日期
 * @param time
 */

export const isDeadLineTime = (time: number, deadLine?: string) => {
  const targetTime = deadLine
    ? new Date(deadLine).getTime()
    : new Date("2022 06 03 00:00").getTime();
  // const targetTime = new Date("2021 05 10 00:00").getTime();
  if (time >= targetTime) {
    return true;
  }
  return false;
};

/**
 * 处理用户选择的预约时间
 * @param time
 */
interface timeObject {
  date: string;
  startTime: string;
  endTime: string;
}
export const handleTime = (options: timeObject) => {
  const startResult = options.date + " " + options.startTime;
  const endResult = options.date + " " + options.endTime;
  return {
    startResult,
    endResult
  };
};


/**
 * 显示不同状态信息
 * @param status
 */
export const showAppointMsg = (status: string) => {
  switch (status) {
    case '2':
    case '3':
    case '4':
    case '-2':
    case '-3':
      return '你已经面试了哦'
    default:
      return '你还没报名哦'
  }
}
