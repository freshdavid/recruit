import React from 'react'
import { View } from '@tarojs/components';
import './index.scss'
import Form from './Form'
/**
 * 报名
 */
const SignUp = () => {

  return (
    <>
      <View className="container">
        <View className="cm">
          <View className='txt'>易动工作室 </View>
        </View>
        <View className="cm">
          <View className="txt">欢迎你!</View>
        </View>
        <Form />
      </View>
    </>
  )
}

export default SignUp;
