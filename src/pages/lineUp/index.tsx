import { View, Image, Text } from "@tarojs/components";
import Taro, { getRealtimeLogManager } from "@tarojs/taro";
import React, { useState, useEffect } from "react";
import { AtButton, AtMessage } from "taro-ui";
import {
  cancelLine,
  getLineCount,
  getUserStatus,
  getWaitQueue,
  wait
} from "../../api/user";
import {
  getLocalStore,
  parse,
  success,
  error,
  formatData,
  confirm,
  setLocalStore
} from "../../utils/util";
import "./index.scss";
export default function LineUp() {
  // 是否展示的是等待的信息
  const [showWating, setShowWating] = useState<boolean>(false);

  // 用户状态
  const [status, setStatus] = useState<string>();

  // 有多少人在排队
  const [count, setCount] = useState<number>(0);

  // 是否取消排队
  const [cancel, setCancel] = useState<boolean>(false);

  const { imgSrc } = getLocalStore("userInfo");
  const openid = getLocalStore("openid");
  // 下拉刷新重新获取数据
  const [fresh, setRefresh] = useState<number>(0);

  // 监听下拉刷新
  Taro.usePullDownRefresh(() => {
    // 重新获取排队人数
    setRefresh(Math.random());

    //重新获取状态
    (async () => {
      const { data } = await getUserStatus(openid);
      console.log("当前用户状态", data);
      // 设置状态
      setStatus(data);
    })();

    //200毫秒 停止下拉刷新
    setTimeout(() => {
      Taro.stopPullDownRefresh();
    }, 1000);
  });

  // 获取排队的人数
  useEffect(() => {
    (async () => {
      const { code, data } = await getWaitQueue(openid);
      if (code == "1" && data) {
        // 格式化队列并过滤出取消排队的人
        console.log(formatData(data));
        let queues = formatData(data).filter(item => !item.state);
        console.log(queues);
        let index = queues.findIndex(item => item.openid === openid);
        console.log("当前用户所在的位置:", index);
        // 设置排队人数
        setCount(index > -1 ? index : queues.length);
        if (index > -1) {
          // 显示等待
          setShowWating(true);
        }
      }
    })();
  }, [showWating, fresh]);

  // 判断是否满足排队的条件
  useEffect(() => {
    (async () => {
      const { data } = await getUserStatus(openid);
      console.log("用户的状态:", data);
      // 设置状态
      setStatus(data);
    })();
  }, []);

  /**
   * 排队
   */
  const waitLine = async () => {
    //订阅消息，这个id是易动工作室的，而不是易动Studio招新的
    // const id = "1ae24siVl2_2qLkLnA-N_p-F7P_pbvp9YL4fGG1RPmw"; //消息模板id
    // console.log(getLocalStore("subscribe") !== true);
    try {
      //避免重复订阅消息,先判断用户是否已经订阅
      // if (getLocalStore("subscribe") !== true) {
      //   //发送订阅信息请求
      //   const res = await Taro.requestSubscribeMessage({
      //     tmplIds: [id]
      //   });
      //   //用户允许订阅消息
      //   if (res[id] === "accept") {
      //     //标记用户已订阅
      //     setLocalStore("subscribe", true);
      //     // 开始排队
      //     const { code, data } = await wait(openid);
      //     if (code == "1") {
      //       success(data);
      //       setShowWating(true);
      //     }
      //   }
      //   //提醒用户允许订阅消息
      //   else {
      //     error("请允许发送消息");
      //   }
      // } else {
      // 开始排队
      const { code, data } = await wait(openid);
      if (code == "1") {
        success(data);
        setShowWating(true);
      }
      // }
    } catch (err) {
      console.log(err);
    }
  };

  /**
   * 取消排队
   */
  const cancelWaitLine = async () => {
    const isConfirm = await confirm("提示", "你确认取消排队吗");
    if (isConfirm) {
      const { code, data } = await cancelLine(openid);
      console.log(code, data)
      if (code == "1") {
        success("取消排队成功!");
        // 显示等待
        setShowWating(false);
      }
    }
  };

  /**
   * 预计需要的时间
   * @param totleTime
   */
  const getTime = (totleTime: number): string => {
    if (totleTime < 60) {
      return `${totleTime}分钟`;
    } else {
      // 多少小时
      const leftTime = Math.floor(totleTime / 60);
      // 多少分钟
      const subTime = totleTime % 60;
      return subTime === 0 ? leftTime + '小时' : leftTime + '小时' + subTime + '分钟'
    }


    // if (totleTime < 60) {
    //   return `${totleTime}分钟`;
    // } else if (totleTime % 60 === 0) {
    //   return `${totleTime / 60}小时`;
    // } else {
    //   return `${Math.floor(totleTime / 60)}小时${totleTime % 60}分钟`;
    // }
  };

  /**
   * 根据状态展示不同的信息
   */
  const statusGenShowMsg = (status: string) => {
    switch (status) {
      // 没报名
      case "0":
        return <Text className=" f-plus">请先去报名哦</Text>;
      // 报名了，可以排队
      case "1":
        return (
          <>
            <View className="l-line">
              <View className="line-msg">
                {// 根据是否等待去显示不同结果
                  !showWating ? (
                    <>
                      {" "}
                      <>
                        当前有<Text className="line-count">{count}</Text>
                        人在排队面试{" "}
                        {
                          <View className="time">
                            {count === 0 ? (
                              "现在没有人排队哦，早点排队吧"
                            ) : (
                              <>
                                预计需要{" "}
                                <Text className="line-count">
                                  {getTime(count * 20)}
                                </Text>{" "}
                              </>
                            )}{" "}
                          </View>
                        }
                      </>
                    </>
                  ) : // 判断当前人数是否为0，为0，则到指定地点去面试
                    count !== 0 ? (
                      <>
                        <View className="wait">你正在排队中...</View>
                        你前面还有
                        <Text className="line-count">{count}</Text>人
                        <View className="cancel-int">
                          <AtButton type="primary" onClick={cancelWaitLine}>
                            取消排队
                          </AtButton>
                        </View>
                      </>
                    ) : (
                      // count为0
                      <>
                        <View className="wait">到你面试啦，接收到通知后请到指定地点面试吧</View>
                        <View className="cancel-int">
                          <AtButton type="primary" onClick={cancelWaitLine}>
                            取消排队
                          </AtButton>
                        </View>
                      </>
                    )}
              </View>
            </View>
            <View className="start-line">
              {/* 当前已经排队并且状态是已报名的 */}
              {!showWating && (
                <AtButton type="primary" onClick={waitLine}>
                  点我开始排队吧
                </AtButton>
              )}
            </View>
          </>
        );
      // 面试了，等待结果
      case "2":
        return <Text className=" f-plus">面试已结束,请耐心等待结果通知吧</Text>;
      //面试通过
      default:
        return <Text className=" f-plus">面试已结束</Text>;
    }
  };

  return (
    <View className="line-up fx-v-c">
      <AtMessage />
      <>
        <View className="avatar">
          <Image src={imgSrc} className="a-img"></Image>
        </View>
        {statusGenShowMsg(status)}
      </>
    </View>
  );
}
