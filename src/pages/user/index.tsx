import React, { Component } from "react";
import { View, Image } from "@tarojs/components";
import List from "../../components/user/List";
import Avatar from "../../components/user/Avatar";
import "taro-ui/dist/style/components/button.scss"; // 按需引入
import "./index.scss";

export default class Index extends Component {
  componentWillMount() {}

  componentDidMount() {}
  componentWillUnmount() {}
  componentDidShow() {}

  componentDidHide() {}

  render() {
    return (
      <>
        <View className="top">
          <View className="center">
            <Avatar />
            {/* 波浪动画 */}
            <Image
              className="gif-wave"
              src="http://docfile.funkingka.cn/FqzmiFqvXbHg-cfbquNRlawHUgHt"
            />
          </View>
        </View>
        <View className="bot fx-c">
          <List />
        </View>
      </>
    );
  }
}
