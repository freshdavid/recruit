import { Component } from "react";
import { View, Text, Swiper, SwiperItem } from "@tarojs/components";
import { AtIcon } from "taro-ui";

import "taro-ui/dist/style/components/button.scss"; // 按需引入
import "./index.scss";

// eslint-disable-next-line import/first
import Taro from "@tarojs/taro";

//组件的引入
import Main from "../../components/description/Main";
import Front from "../../components/description/Front";
import BackEnd from "../../components/description/BackEnd";

export default class Index extends Component {
  state = {
    currentIndex: 0
  };

  componentWillUnmount() {}

  componentDidMount() {}

  componentDidShow() {}
  //动态地载入组件（防止动画提前加载）
  change = event => {
    //获取当前的滑块index，
    const index = event.detail.current;
    setTimeout(() => {
      //修改currentIndex并加载相应的组件
      this.setState({ currentIndex: index });
    }, 500);
  };
  componentDidHide() {}
  render() {
    return (
      <Swiper
        className="description-container"
        indicatorColor="#999"
        indicatorActiveColor="#333"
        vertical={true}
        skipHiddenItemLayout={true}
        style={{ height: "100vh" }}
        onChange={this.change}
      >
        <SwiperItem>
          <View className="demo-text">
            {this.state.currentIndex == 0 && <Main />}
          </View>
        </SwiperItem>
        <SwiperItem>
          <View className="demo-text">
            {this.state.currentIndex == 1 && <Front />}
          </View>
        </SwiperItem>
        <SwiperItem>
          <View className="demo-text">
            {this.state.currentIndex == 2 && <BackEnd />}
          </View>
        </SwiperItem>
      </Swiper>
    );
  }
}
