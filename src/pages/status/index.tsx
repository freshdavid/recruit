import { View,Text } from '@tarojs/components'
import React, { useState, useEffect } from 'react'
import { AtSteps } from 'taro-ui'
import { getUserStatus } from '../../api/user';
import { $on, formatCurrent, genAssessmentMsg, genInterviewMsg, genSigningMsg, getLocalStore, isLogin } from '../../utils/util';
import './index.scss'
import Taro from '@tarojs/taro'
export default function Status() {
  // 进度条的当前进度
  const [current, setCurrent] = useState<number>(0);
  // 刷新重新获取状态
  const [refresh, setRefresh] = useState<number>(0);

  // 进度条信息
  const items = [
    genSigningMsg(current),
    genInterviewMsg(current),
    genAssessmentMsg(current)
  ]
  // 获取用户的状态
  const info = getLocalStore('userInfo')

  // 监听下拉刷新
  Taro.usePullDownRefresh(() => {
    // 重新获取状态
    setRefresh(Math.random());
    //200毫秒 停止下拉刷新
    setTimeout(() => {
      Taro.stopPullDownRefresh();
    }, 1000)
  })
  // 获取用户信息
  useEffect(() => {
    (async () => {
      const { code, data, message } = await getUserStatus(getLocalStore('openid'));
      console.log({ code, data: +data, message }, '=====');
      if (code == '1') {// 如果报名了,改变状态
        setCurrent(+data);
      }
    })()
  }, [refresh]);

  // 监听，如果报名成功等更新状态
  $on('signCb', () => {
    console.log('更新状态')
    setRefresh(Math.random())
  });
  let st = formatCurrent(current);

  // if (!isLogin()) {
  //   // 如果没有授权登录，默认就为第一阶段
  //   st = 0;
  // }


  return (
    <View className='notify fx-c'>
      {/* 步骤条 */}
      <View className="step">
        {
          info && info.nickName ? (<View className="user-greet">
            <View className="nickname fx-c"><Text className="ofh">{info.nickName}</Text>, 你的当前状态如下</View>
            {/* <View className="nickname">{info.nickName}你好，你的当前状态如下</View> */}
          </View>) : (<View className="user-greet">
            <View className="nickname">你还没登录，请先去登录</View>
          </View>)
        }
        <AtSteps
          items={items}
          current={st}
        />
      </View>
    </View>
  )
}

