import { View, Image, Text } from "@tarojs/components";
import React, { useEffect, useState } from "react";
import { AtIcon, AtButton, AtCurtain } from "taro-ui";
import { getUserInfoById, getUserStatus } from "../../api/user";
import { getLocalStore } from "../../utils/util";
import Taro from "@tarojs/taro";
import "./index.scss";
export default function Result() {
  const userInfo = getLocalStore("userInfo");
  const openid = getLocalStore("openid");
  // 控制显示二维码
  const [isOpened, setIsOpened] = useState<boolean>(false);

  // 是否显示结果消息
  const [isShow, setIsShow] = useState<boolean>(false);

  // 状态
  const [status, setStatus] = useState<string>();

  // 是否显示成功的图表和按钮
  const [isSuccess, setIsSuccess] = useState<boolean>(false);

  const [msg, setMsg] = useState<string>("");
  // 二维码地址
  const [imgSrc, setImgSrc] = useState<string>('');
  const passInfo = {
    passIn:
      "恭喜你通过了我们的面试，如果想继续参加考核，请扫描下方二维码加入对应QQ群吧",
    noPassIn:
      "非常抱歉，你没能通过我们的面试，但你很出色，要一直保持一颗学习的心哦",
    passAcss: "恭喜你通过了我们的考核",
    noPassAcss:
      "非常抱歉，你没能通过我们的考核，但你很出色，要一直保持一颗学习的心哦"
  };

  // 获取信息判断前后端
  useEffect(() => {
    (async () => {
      const { code, data } = await getUserInfoById(openid);
      if (code == "1") {
        if (data[0]) {
          // 根据前后端选择不同的路径
          console.log(data[0].direction);
          const imgUrl =
            data[0].direction === "后台"
              ? "https://www.shangzhuxiaofu.cn:8080/photo/school/37b28f5b8d6e4f55bebda6b61cfd9909.jpg"
              : "https://www.shangzhuxiaofu.cn:8080/photo/school/710bc222120743368d9bee0b44286ff2.jpg";
          imgUrl && setImgSrc(imgUrl);
        }
      }
    })();
  }, []);

  /**
   * 请求查看通过的状态
   */
  useEffect(() => {
    (async () => {
      const { code, data } = await getUserStatus(openid);
      if (code == "1") {
        let flag = true;
        // 成功
        if (data == "3") {
          // 面试通过
          setMsg(passInfo.passIn);
          setIsSuccess(true); // 显示成功的消息
        } else if (data == "-2") {
          //  面试不通过
          setMsg(passInfo.noPassIn);
        } else if (data == "4") {
          // 考核通过
          setMsg(passInfo.passAcss);
          setIsSuccess(true);
        } else if (data == "-3") {
          // 考核不通过
          setMsg(passInfo.noPassAcss);
        } else {
          // 不是上述的情况，都不显示
          flag = false;
        }
        setIsShow(flag);
        setStatus(data);
      }
    })();
  }, []);

  // 关闭二维码
  const close = () => {
    setIsOpened(false);
  };

  // 展示二维码
  const openQRImg = () => {
    // setIsOpened(true)
    Taro.previewImage({
      current: imgSrc, // 当前显示图片的http链接
      urls: [imgSrc] // 需要预览的图片http链接列表
    });
  };

  return (
    <>
      <AtCurtain isOpened={isOpened} onClose={close}>
        {
          imgSrc && <Image style="width:100%;height:600rpx" src={imgSrc} />
        }
      </AtCurtain>
      <View className="result-notify fx-c">
        {isShow ? (
          <>
            <View className="status-logo">
              {isSuccess ? (
                <AtIcon
                  value="check-circle"
                  size="48"
                  color="rgb(49, 226, 49)"
                ></AtIcon>
              ) : (
                <AtIcon value="close-circle" size="48" color="#f40"></AtIcon>
              )}
            </View>
            <View className="msg">
              {userInfo.nickName + " ,"} {msg}
            </View>
            {isSuccess && status !== "4" && (
              <View className="res-btn">
                <AtButton type="primary" onClick={openQRImg}>
                  查看二维码
                </AtButton>
              </View>
            )}
          </>
        ) : (
          <Text>暂时还没有消息哦</Text>
        )}
      </View>
    </>
  );
}
