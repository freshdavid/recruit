import { Component } from "react";
import "./app.scss";
import { getRes } from "./utils/request";
import Taro from "@tarojs/taro";
import { setLocalStore, validateLoginStatus } from "./utils/util";
import { LoginRes } from "./api/types";
import { toLogin } from "./api/user";

class App extends Component {
  componentDidMount() {
    (async () => {
      // 静默登录
      // 1. 检验登录态
      const { code } = await validateLoginStatus();

      // 2. 根据登录态判断是否需要重新登录
      if (code === 0) {
        // 重新登录
        console.log("登录~~~~~~~~");
        const { code } = await Taro.login();
        console.log(code);

        const { code: resCode, data: openid } = await toLogin(code);
        console.log(resCode, openid);
        if (resCode == "1") {
          // 登录成功
          // 存储openid
          setLocalStore("openid", openid);
        }
      }
      //转发
      Taro.showShareMenu({
        showShareItems: ["wechatFriends"]
      });
    })();
  }
  render() {
    return this.props.children;
  }
}

export default App;
